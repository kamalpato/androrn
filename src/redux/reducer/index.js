import {combineReducers} from 'redux';
import {AuthReducer} from './AuthReducer';
import {UserReducer} from './UserReducer';
// import {ProjectReducer} from './ProjectReducer';
// import {TaskReducer} from './TaskReducer';
// import themeReducer from './themeReducer';

const reducer = combineReducers({
  auth: AuthReducer,
  User: UserReducer,
//   project: ProjectReducer,
//   task: TaskReducer,
  // theme: themeReducer,
});

export default reducer;