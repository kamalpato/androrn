import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

// import reducer from './Reducer/index';

let middlewares = applyMiddleware(thunk);

export const store = createStore(
    // reducer, 
    middlewares);