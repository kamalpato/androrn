import React from 'react';
import { View , Text} from 'react-native';
import { useNavigation } from '@react-navigation/native';
// import Text from 'react-native-paper';

const HomeScreen = () => {
const navigation = useNavigation();
    return (

        <View style={{justifyContent:'center', alignSelf:'center'}}>
            <Text>Home Screen</Text>
        </View>
    );
};

export default HomeScreen;
