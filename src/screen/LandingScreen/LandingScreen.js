import React from 'react';
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';

import Starter from '../../assets/icon/Yasbutilogo.png'
import { LinearConfig } from '../../LinearConfig';

const LandingScreen = () => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor='#FFF' barStyle="dark-content" />
      <View style={styles.slide}>
          <Image source={Starter} style={styles.logo} />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title="Log In"
          containerStyle={styles.button}
          ViewComponent={LinearGradient}
          linearGradientProps={LinearConfig}
          titleStyle={styles.buttonTitle}
          onPress={() => navigation.navigate('Login Screen')}
        />
        <Button
          title="Register"
          containerStyle={[styles.button]}
          titleStyle={[styles.buttonTitle, { color: '#1589CB' }]}
          type="outline"
          onPress={() => navigation.navigate('Register Screen')}
        />
      </View>
      <View style={styles.termText}>
        <Text style={{ textAlign: 'center' }}>
          <Text>By creating an account, you agree to our</Text>
          <Text style={{ fontWeight: 'bold' }}> Terms of Service</Text>
          <Text> and</Text>
          <Text style={{ fontWeight: 'bold' }}> Privacy Policy</Text>.
        </Text>
      </View>
    </View>
  );
};

export default LandingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    backgroundColor: '#F4F4F4',
    borderWidth: 1,
    borderColor: '#FAFAFA',
    marginBottom: 13,
    marginHorizontal: 16,
    borderRadius: 3,
    width: 120,
  },
  buttonTitle: {
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    fontSize: 18,
    marginVertical: 5,
    textAlign: 'center',
  },
  termText: {
    fontFamily: 'Montserrat',
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 50,
    marginBottom: 8,
    color: '#07689F',
    marginHorizontal: 16,
  },
  bodyText: {
    fontFamily: 'Montserrat',
    fontSize: 18,
    marginBottom: 16,
    textAlign: 'center',
    color: '#07689F',
    marginHorizontal: 16,
  },
  slide: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 320,
    height: 220,
    marginTop: 50,
    marginBottom: 206,
    resizeMode: 'center'
  },
});
