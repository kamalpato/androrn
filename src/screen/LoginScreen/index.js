import React from 'react';
import { Button } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import { View, Text, Image, StatusBar, StyleSheet } from 'react-native';

import { LinearConfig } from '../../LinearConfig';


const LoginScreen = () => {
    const navigation = useNavigation();
    
    return (
        <View>
            <Text>Login Screen</Text>
            <View style={styles.buttonContainer}>
                <Button
                    title="Log In"
                    containerStyle={styles.button}
                    ViewComponent={LinearGradient}
                    linearGradientProps={LinearConfig}
                    titleStyle={styles.buttonTitle}
                    onPress={() => navigation.navigate('BottomTab')}
                />
            </View>
        </View>
    );
};
export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        justifyContent: 'center',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    button: {
        backgroundColor: '#F4F4F4',
        borderWidth: 1,
        borderColor: '#FAFAFA',
        marginBottom: 13,
        marginHorizontal: 16,
        borderRadius: 3,
        width: 120,
    },
    buttonTitle: {
        fontFamily: 'Montserrat',
        fontWeight: 'bold',
        fontSize: 18,
        marginVertical: 5,
        textAlign: 'center',
    },
})
