import React, { useEffect } from 'react';
import { StyleSheet } from 'react-native';
import  {Icon}  from 'react-native-elements';
import { useSelector, useDispatch } from 'react-redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';

// import Avatar from './assets/icon/avatar.png';
import HomeScreen from './screen/BottomTab/HomeScreen';
import LiveScreen from './screen/BottomTab/LiveScreen';
import DiscussionScreen from './screen/BottomTab/DiscussionScreen';
import ProfileScreen from './screen/BottomTab/ProfileScreen';

// import { getUser } from './redux/Action/ShowUserAction';

const Tab = createMaterialBottomTabNavigator();

const RootTab = () => {
    // const profile = useSelector(state => state.showUser);
    // const dispatch = useDispatch();
    // const image = profile.imageProfile;

    // useEffect(() => {
    //     dispatch(getUser());
    // }, [dispatch]);

    // let avatar;

    // if (image == null) {
    //     avatar = Avatar;
    // } else {
    //     avatar = { uri: image }
    // }
    return (
        <Tab.Navigator
            tabBarOptions={{
                activeBackgroundColor: '#000',
                tabStyle: styles.tabItem,
                style: styles.tabContainer,
            }}
            initialRouteName=" Home ">
            <Tab.Screen
                name="Home Screen"
                component={HomeScreen}
                options={{
                    title: 'Home',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Icon
                                type="feather"
                                name="home"
                                size={25}
                                color={focused ? '#FFF' : '#7B7B7B'}
                            />
                        );
                    },
                }}
            />
            <Tab.Screen
                name="Live"
                component={LiveScreen}
                options={{
                    title: 'Live',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Icon
                                type="feather"
                                name="youtube"
                                size={25}
                                color={focused ? '#FFF' : '#7B7B7B'}
                            />
                        );
                    },
                }}
            />
            <Tab.Screen
                name="Discussion"
                component={DiscussionScreen}
                options={{
                    title: 'Discussion',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Icon
                                type="feather"
                                name="message-square"
                                size={25}
                                color={focused ? '#FFF' : '#7B7B7B'}
                            />
                        );
                    },
                }}
            />
            <Tab.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    title: 'Profile',
                    tabBarIcon: ({ focused }) => {
                        return (
                            <Icon
                                type="feather"
                                name="user"
                                size={25}
                                color={focused ? '#FFF' : '#7B7B7B'}
                            />
                        );
                    },
                }}
            />
        </Tab.Navigator>
    );
};

export default RootTab;

const styles = StyleSheet.create({
    tabContainer: {
        justifyContent: 'center',
        height: 70,
        alignItems: 'center',
        backgroundColor: '#FAFAFA',
        borderTopWidth: 1,
        borderTopColor: '#000'
    },
    tabItem: {
        alignSelf: 'center',
        margin: 45,
        height: 40,
        // backgroundColor:'yellow',
        borderRadius: 3,
    },
    profileImage: {
        width: 30,
        height: 30,
        borderRadius: 100,
    },
});
