import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';

// import RootStack from './RootStackNavigator';
import RootStackNavigator from './RootStackNavigator';

const AppNavigator = () => {
    return (
        <NavigationContainer>
            <RootStackNavigator/>
        </NavigationContainer>
    );
};

export default AppNavigator;