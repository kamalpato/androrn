import React from 'react';
import { StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

import RootTab from './RootTabNavigator';
import SplashScreen from './screen/SplashScreen';
import LandingScreen from './screen/LandingScreen/LandingScreen';
import RegisterScreen from './screen/RegisterScreen';
import LoginScreen from './screen/LoginScreen';

const Stack = createStackNavigator();

const RootStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName="SplashScreen" headerMode='screen'>
            <Stack.Screen
                name="Splash Screen"
                component={SplashScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Landing Screen"
                component={LandingScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Register Screen"
                component={RegisterScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="Login Screen"
                component={LoginScreen}
                options={{ headerShown: false }}
            />
            <Stack.Screen
                name="BottomTab"
                component={RootTab}
                options={{ headerShown: false }}
            />
        </Stack.Navigator>
    );
};
export default RootStackNavigator;

